# PlayCD

Command line Audio CD Player


## Contributions

NLS specific corrections, updates and submissions should not be 
directly to submitted this project. NLS is maintained at the [FD-NLS](https://github.com/shidel/fd-nls)
project on GitHub. If the project is still actively maintained by it's
developer, it may be beneficial to also submit changes to them directly.

## PLAYCD.LSM

<table>
<tr><td>title</td><td>PlayCD</td></tr>
<tr><td>version</td><td>2022-10-07</td></tr>
<tr><td>entered&nbsp;date</td><td>2023-02-20</td></tr>
<tr><td>description</td><td>Command line Audio CD Player</td></tr>
<tr><td>keywords</td><td>music, audio, cd</td></tr>
<tr><td>author</td><td>Andreas "Japheth" Grech</td></tr>
<tr><td>maintained&nbsp;by</td><td>Andreas "Japheth" Grech</td></tr>
<tr><td>primary&nbsp;site</td><td>https://github.com/Baron-von-Riedesel/PlayCD</td></tr>
<tr><td>platforms</td><td>DOS</td></tr>
<tr><td>copying&nbsp;policy</td><td>Public Domain</td></tr>
</table>
